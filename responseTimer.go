package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"time"
)

func ResponseTime() time.Duration {
	// dialing a TCP connection to the webpage
	conn, err := net.Dial("tcp", "gitlab.com:http")
	if err != nil {
		panic(err)
	}
	defer conn.Close()
	conn.Write([]byte("GET / HTTP/1.0\r\n\r\n"))
	// Starting a timer to measure the response time of the webpage
	start := time.Now()
	oneByte := make([]byte, 1) // If we need the response time for the first Byte from the webpage server
	_, err = conn.Read(oneByte)
	if err != nil {
		panic(err)
	}
	// Reading all response from the server to determine the response time. (The response is ignored as we only need the time)
	_, err = ioutil.ReadAll(conn)
	if err != nil {
		panic(err)
	}
	responseTime := time.Since(start)
	return responseTime
}

// The Log file writer to log all the response times to the webpage over 5 minutes.
func WriteLogFile(ResponseTimer []time.Duration) {
	f, err := os.Create("LogFile.txt")
	if err != nil {
		log.Fatal("The Log File could not be created: ", err)
	}
	defer f.Close()
	if err == nil {
		for _, s := range ResponseTimer {
			f.WriteString(fmt.Sprintf("The HTTP Response Time to gitlab.com website is : %s", s.String()))
			f.WriteString("\n")
		}
		f.Sync()
	}
}

func main() {
	var LastbyteResponseTimer []time.Duration
	timer := time.Now() // Timer needed to register the current time for logging the response times of the webpage over a fixed period of time.
	fmt.Printf("Fetching response times.......")
	for time.Since(timer).Minutes() <= 5.0 {
		LastbyteResponseTimer = append(LastbyteResponseTimer, ResponseTime())
	}
	fmt.Println("Fetching completed. \n Check the Log File!")
	WriteLogFile(LastbyteResponseTimer)
}
