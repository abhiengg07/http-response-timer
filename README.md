A HTTP Response Timer to log HTTP reponse time to GET request to https://gitlab.com over 5 minutes.

Usage:

```
 go run responseTimer.go

```

The code will perform HTTP GET to https://gitlab.com and records the response times into a log file. I used Go's net package to estalish a tcp connection to the server as we can get the most accurate response time compared to just timing the normal GET request as it includes waiting time, DNS lookup time etc., into the process which is not the part of HTTP response time.


Explanation of Response Time : 

The response time shows the total amount of time needed to perform a specific test. It can be divided in five parts: 

DNS lookup time - the time needed to resolve the targets hostname to an IP address. High DNS lookup times might indicate a problem with your DNS servers.
For example if the DNS lookup time is higher than 10 seconds, it is very probable that your primary DNS server is unreachable or is timing out.
Please notice that if you have entered an IP address in the hostname configuration field, no DNS lookup will be performed and this time will be close to 0. 

Connection time - the time needed to create a connection to your server. This is the main index for measuring network latency.
High connection times might indicate network or routing problems. 

Redirect time - the time it takes to follow any HTTP redirects in the HTTP servers response. The redirect time will also include the time needed to perform any DNS lookups or connection times that might occur during this process. 

First byte - the time, in seconds, it took from the moment the connection was established until the first byte is just about to be transferred. This includes the time to perform any negotiations with the server and also the time the server needs to calculate the result.
When this value is too high it is most likely that there is a problem with your server load or that the page you are monitoring requires a lot of time before any response is sent back to the client. 

Last byte - the time in seconds it took to download the final server response. This value is a bandwidth indicator - if it is too high you should consider upgrading your bandwidth, to increase the download speed from your site or server. This parameter is not applicable to the Basic monitoring where no data is downloaded from the server. 


The responseTimer.go computes the Last byte Time in miliseconds.  

